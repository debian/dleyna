.. SPDX-License-Identifier: LGPL-2.1-or-later

.. dleyna-renderer-service(1):

=======================
dleyna-renderer-service
=======================

----------------------------------------------------------
Accessing DLNA renderers on the network
----------------------------------------------------------

:Manual section: 1

SYNOPSIS
========

    **dleyna-renderer-service**

DESCRIPTION
===========

    Users or administrators should never need to start this daemon as it will
    be automatically started by dbus-daemon whenever a process sends a D-Bus message
    to the *com.intel.dleyna-renderer* name on the session bus.

    *dleyna-renderer-service* can discover and be used for control of DLNA media renderer
    devices on the local network.

OPTIONS
=======

    This command does not have any commandline options

ENVIRONMENT
===========

    *G_MESSAGES_DEBUG*
        Enable additional debug messages from the used libraries. Either *all* for all
        debug messages or a pattern for matching a subset of messages. The available
        message categories heavily depend on the library.

    *GUPNP_DEBUG*
        If set, GUPnP will print the content of SOAP calls

    *DLEYNA_CONNECTOR_PATH*
        Can be used to override the compile-time search path for the IPC connector

BUGS
====

    Please send bug reports to either the distribution bug tracker
    or the upstream bug tracker at https://gitlab.gnome.org/World/dLeyna/issues

SEE ALSO
========

    dleyna-renderer-service.conf(5)
