==========
References
==========

.. [MPRIS] MRPIS D-Bus Interface Specification (http://specifications.freedesktop.org/mpris-spec/latest/)
.. [DLNA] DLNA Guidelines December 2011, Part 1 Architectures and Protocols (http://www.dlna.org/)
.. [CM2] ConnectionManager:2 Service Template (http://www.upnp.org/)
.. [MS2S] MediaServer2Spec (https://wiki.gnome.org/Projects/Rygel/MediaServer2Spec)
.. [CIPARAM] 7.4.1.3.22 MM ci-param (Conversion Indicator Flag) in [DLNA]_
.. [FLAGSPARAM] 7.4.1.3.23 MM flags-param (Flags Parameter) in [DLNA]_
.. [OPPARAM] 7.4.1.3.19 MM op-param (Operations Parameter for HTTPS) in [DLNA]_
