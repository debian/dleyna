==================
The Manager Object
==================

There is only ever a single instance of this object.  The manager
object exposes two D-Bus interfaces:

1. ``com.intel.dLeynaServer.Manager``
2. ``org.freedesktop.DBus.Properties``

********************************
com.intel.dLeynaRenderer.Manager
********************************

Methods
=======

The interface ``com.intel.dLeynaRenderer.Manager`` contains four
methods.  Descriptions of each of these methods along with their D-Bus
signatures are given below.

GetRenderers() -> ao
--------------------

    GetRenderers takes no parameters and returns an array of D-Bus object
    paths.  Each of these paths reference a D-Bus object that represents a
    single DMR.

GetVersion() -> s
-----------------

    Returns the version number of dleyna-renderer-service

Release() -> void
-----------------

    Indicates to dleyna-renderer-service that a client is no longer
    interested in its services.  Internally, dleyna-renderer-service
    maintains a reference count.  This reference count is increased when a
    new client connects.  It is decreased when a client quits.  When the
    reference count reaches 0, dleyna-renderer-service exits.  A call to
    Release also decreases the reference count.  Clients should call this
    method if they intend to keep running but they have no immediate plans
    to invoke any of dleyna-renderer-service's methods.  This allows
    dleyna-renderer-service to quit, freeing up system resources.

Rescan() -> void
-----------------

    Forces a rescan for DMRs on the local area network.  This is useful to
    detect DMRs which have shut down without sending BYEBYE messages or to
    discover new DMRs which for some reason were not detected when either
    they, or the device on which dLeyna-renderer runs, was started or joined
    the network.  New in version 0.0.2.


Properties
==========

The ``com.intel.dLeynaRenderer.Manager`` interface exposes information via a number
of D-Bus properties.  These properties are described below:

+-------------------+-----------+-----------+-----------------------------------------+
|     Name          |   Type    |m/o [#f1]_ |              Description                |
+===================+===========+===========+=========================================+
| NeverQuit         |     b     | m         | True if the service always stay in      |
|                   |           |           | memory running.  False if the service   |
|                   |           |           | quits when the last client disconnects. |
+-------------------+-----------+-----------+-----------------------------------------+
| WhiteListEntries  |     as    | m         | The list of entries that compose the    |
|                   |           |           | white list used to filter the networks. |
|                   |           |           | An Entry could be an interface name     |
|                   |           |           | (eth0), an ip address (127.0.0.1) or    |
|                   |           |           | a SSID (MyWiFi)                         |
+-------------------+-----------+-----------+-----------------------------------------+
| WhiteListEnabled  |     b     | m         | True if the Network Filtering is active.|
+-------------------+-----------+-----------+-----------------------------------------+

A ``org.freedesktop.DBus.Properties.PropertiesChanged`` signal is emitted when
these properties change. These properties can be changed using the ``Set()`` method of
``org.freedesktop.DBus.Properties`` interface.

Signals
=======

The ``com.intel.dLeynaRenderer.Manager`` interface also exposes two signals.

FoundRenderer(o)
----------------
    Is generated whenever a new DMR is detected on the local area network.
    The signal contains the path of the newly discovered renderer.

LostRenderer(o)
---------------
    Is generated whenever a DMR is shutdown.  The signal contains the path
    of the renderer which has just been shutdown.


.. rubric:: Footnotes

.. [#f1] m/o indicates whether the property is optional or mandatory.
