dLeyna-Renderer
===============

.. toctree::
   :maxdepth: 3
   :caption: Contents:

   manager
   renderer

dleyna-renderer-service is a middleware component. It is designed to be
launched by D-Bus activation when needed. It automatically shuts down
when the last of its clients quits or releases its connection.

dleyna-renderer-service currently connects to the D-Bus session bus,
although this may change in the future.  It exposes two different
types of objects:

1. A manager object.  There is only ever a single instance of this
object.  It can be used to retrieve a list of the :term:`DMR` on the local
area network.  It is also used to perform certain renderer independent
tasks.

2. Renderer objects.  One separate object is exposed for each :term:`DMR`
available on the :term:`LAN`.  These objects expose interfaces that allow
clients to retrieve information about the renderers, to manipulate
them and to push content to them.
