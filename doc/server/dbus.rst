dLeyna-Server
=============

.. toctree::
   :maxdepth: 3
   :caption: Contents:

   manager
   server
   container
   item

dleyna-server-service is a middleware component.  It is designed to be
launched by D-Bus activation when needed.  It automatically shuts down
when the last of its clients quits or releases its connection.

dleyna-server-service currently connects to the D-Bus session bus,
although this may change in the future.  It exposes four different
types of objects:

1. A manager object.  There is only ever a single instance of this
object.  It can be used to retrieve a list of the DMSs on the local
area network.  It is also used to perform certain server independent
tasks.

2. Server objects.  One separate object is exposed for each DMS
available on the LAN.  These objects expose interfaces that allow
clients to retrieve information about the servers and to browse and
search their contents.

3. Container objects.  Container objects represent separate folders
within the DMS.  They allow clients to browse and search the contents
of the folder to which they correspond.

4. Item objects.  Represent individual pieces of media content
published by a DMS.  These objects can be used to retrieve information
about media objects, such as their name, their authors, and most
importantly, their URLs.

