include:
    - remote: "https://gitlab.freedesktop.org/freedesktop/ci-templates/-/raw/0c312d9c7255f46e741d43bcd1930f09cd12efe7/templates/ci-fairy.yml"
    - remote: 'https://gitlab.freedesktop.org/freedesktop/ci-templates/-/raw/0c312d9c7255f46e741d43bcd1930f09cd12efe7/templates/fedora.yml'

variables:
    MESON_TEST_TIMEOUT_MULTIPLIER: 3

stages:
    - review
    - prepare
    - build
    - test
    - analysis
    - website

.check-template: &check
  extends:
    - .fdo.ci-fairy
  artifacts:
    expire_in: 1 week
    paths:
      - check-junit-report.xml
    reports:
      junit: check-junit-report.xml

check-commit-log:
  variables:
    GIT_DEPTH: "100"
  stage: review
  script:
    - if [[ x"$CI_MERGE_REQUEST_TARGET_BRANCH_NAME" != "x" ]] ;
      then
        ci-fairy check-commits --junit-xml=check-junit-report.xml ;
      else
        echo "Not a merge request" ;
      fi
  <<: *check

check-merge-request:
  variables:
    GIT_STRATEGY: none
  stage: review
  script:
    - if [[ x"$CI_MERGE_REQUEST_TARGET_BRANCH_NAME" != "x" ]] ;
      then
        ci-fairy check-merge-request --require-allow-collaboration --junit-xml=check-junit-report.xml ;
      else
        echo "Not a merge request" ;
      fi
  <<: *check

.build-template: &build
  stage: build
  script:
      - meson . build --prefix=/usr -Dlog_level=8 -Denable_debug=true -Dlog_type=glib -Db_coverage=true
      - ninja -C build
  artifacts:
      expire_in: 1 day
      paths:
          - build

.dleyna.fedora@common:
  variables:
    BASE_TAG: '2022-08-29.0'
    FDO_UPSTREAM_REPO: World/dLeyna
    FDO_DISTRIBUTION_PACKAGES: 'clang clang-analyzer gcovr git libasan libubsan python3-gobject python3-pip xmlto gobject-introspection-devel gtk-doc libsoup3-devel libuuid-devel libxml2-devel ninja-build gupnp-dlna-devel python3-sphinx python3-docutils'
    FDO_DISTRIBUTION_EXEC: |
      dnf clean all &&
      pip3 install meson &&
      pip3 install sphinx_rtd_theme

.dleyna.fedora:36@x86_64:
  extends: .dleyna.fedora@common
  variables:
    FDO_DISTRIBUTION_VERSION: 36
    FDO_DISTRIBUTION_TAG: "x86_64-${BASE_TAG}"

build-fedora-container@x86_64:
  extends:
    - .fdo.container-build@fedora
    - .dleyna.fedora:36@x86_64
  stage: prepare
  variables:
    GIT_STRATEGY: none


build-fedora@x86_64:
    extends:
        - .fdo.distribution-image@fedora
        - .dleyna.fedora:36@x86_64
    needs:
        - build-fedora-container@x86_64
    <<: *build


.test-template: &test
  stage: test
  variables:
    G_SLICE: "always-malloc"
    MALLOC_CHECK_: "3"
  script:
    - cd build
    - |
      # Remove the many "CI_" variables from the environment. Meson dumps the
      # whole environment for every failed test, and that gives a whole
      # screenful of junk each time unless we strip these.
      unset $(env|grep -o '^CI_[^=]*')
      env LANG=C.UTF-8 LC_ALL=C.UTF-8 meson test --print-errorlogs ${MESON_TEST_EXTRA_ARGS}
  after_script:
    - |
      echo "Distribution: "
      echo
      egrep '^NAME=|^VERSION=' /etc/os-release
      echo
      echo "Test suite settings:"
      echo
      echo "G_MESSAGES_DEBUG: ${G_MESSAGES_DEBUG}"
      echo "MESON_TEST_EXTRA_ARGS: ${MESON_TEST_EXTRA_ARGS}"
      echo
      echo "These values can be set at https://gitlab.gnome.org/World/dLeyna/pipelines/new"
  artifacts:
    expire_in: 1 day
    when: always
    paths:
    - build
    reports:
      junit: "build/meson-logs/testlog.junit.xml"

test-fedora@x86_64:
  tags:
    - ipv6
  extends:
    - .fdo.distribution-image@fedora
    - .dleyna.fedora:36@x86_64
  needs:
    - build-fedora@x86_64
  <<: *test

  #trigger-rygel:
  #stage: analysis
  #needs:
  #  - test-fedora@x86_64
  #trigger: GNOME/rygel
  #only:
  #  - master

#coverage-analysis:
#  extends:
#    - .fdo.distribution-image@fedora
#    - .dleyna.fedora:36@x86_64
#  stage: analysis
#  allow_failure: true
#  script:
#    - cd build
#    - mkdir -p coveragereport
#    - gcovr --html-details --print-summary --root=.. --exclude=../build --exclude=../subprojects --exclude=../docs/reference --exclude=../tests --exclude=../tools --exclude=../examples --output coveragereport/index.html
#  coverage: '/^lines: (\d+\.\d+\%)/'
#  artifacts:
#    when: always
#    paths:
#    - build/coveragereport
#  needs:
#    - test-fedora@x86_64

static-scan:
  extends:
    - .fdo.distribution-image@fedora
    - .dleyna.fedora:36@x86_64
  stage: analysis
  needs:
    - build-fedora-container@x86_64
  script:
    - meson --buildtype=debug _scan_build
    - ninja -C _scan_build scan-build
  artifacts:
    paths:
      - _scan_build/meson-logs
  allow_failure: true

coverity:
  extends:
    - .fdo.distribution-image@fedora
    - .dleyna.fedora:36@x86_64
  stage: analysis
  allow_failure: true
  script:
    - curl https://scan.coverity.com/download/linux64 --data "token=$COVERITY_TOKEN&project=dLeyna" --output /tmp/coverity_tool.tgz
    - tar zxf /tmp/coverity_tool.tgz
    - mkdir coverity-build
    - cd coverity-build
    - env CC=clang meson ..
    - ../cov-analysis-linux64-*/bin/cov-build --dir cov-int ninja
    - tar czf cov-int.tar.gz cov-int
    - curl https://scan.coverity.com/builds?project=dLeyna
      --form token=$COVERITY_TOKEN --form email=mail@jensge.org
      --form file=@cov-int.tar.gz --form version="`git describe --tags`"
      --form description="gitlab CI build"
  needs:
    - build-fedora-container@x86_64
  only:
    - master
  except:
    changes:
      - po/*.po

pages:
  extends:
    - .fdo.distribution-image@fedora
    - .dleyna.fedora:36@x86_64
  stage: website
  script:
      - meson doc-build
      - ninja -C doc-build doc/docs
      - mkdir -p public
      - mv doc-build/doc/docs public/docs
  artifacts:
    paths:
      - public
  needs:
    - build-fedora-container@x86_64
  only:
    - master
    - /^wip\/.*docs.*$/

